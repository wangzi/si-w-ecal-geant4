//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file RunData.cc
/// \brief Implementation of the RunData class

#include "RunData.hh"
#include "Analysis.hh"

#include "G4RunManager.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunData::RunData() 
 : G4Run(),
   fVolumeNames{ { "Absorber", "Gap" } }
{
  for ( auto& edep : fEdep ) { 
    edep = 0.; 
  }
  for ( auto& trackLength : fTrackLength ) {
    trackLength = 0.; 
  }

  fEnergy_Time.clear();
  vec_cell_id.clear();
  vec_energy.clear();
  vec_time.clear();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunData::~RunData()
{;}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunData::FillPerEvent()
{
  // get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

  std::cout << " fEnergy_Time size : " << fEnergy_Time.size() << std::endl;
  for( auto &my_pair : fEnergy_Time ){
      //std::cout << "eventID : " << eventID << " name : " << my_pair.first 
          //<< " energy : " << (my_pair.second)[0] << " time : " << (my_pair.second)[1] << std::endl;

      if( (my_pair.second)[0]<0.1 ) { continue; }

      analysisManager->FillNtupleIColumn(1, 0, eventID );
      analysisManager->FillNtupleSColumn(1, 1, my_pair.first );
      analysisManager->FillNtupleFColumn(1, 2, (my_pair.second)[0] );
      analysisManager->FillNtupleFColumn(1, 3, (my_pair.second)[1] );
  
      analysisManager->AddNtupleRow(1);  
  }

  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunData::Update_Energy_Time( G4String volume_name, G4float step_energy, G4float step_time ){

    //std::cout << "name : " << volume_name << " energy: " << step_energy << " time : " << step_time << std::endl;

    std::vector< G4float > tmp;
    tmp.push_back(step_energy);
    tmp.push_back(step_time);
    auto location = fEnergy_Time.find(volume_name);
    if( location == fEnergy_Time.end() ){
        fEnergy_Time.insert( make_pair(volume_name, tmp) );
    
        //std::cout << "If energy: " << step_energy << " time : " << step_time << std::endl;
    }
    else{
   
        G4float old_energy = (fEnergy_Time.at(volume_name))[0];
        G4float old_time = (fEnergy_Time.at(volume_name))[1];
        fEnergy_Time.at(volume_name).clear();
        fEnergy_Time.at(volume_name).push_back( old_energy + step_energy );
        fEnergy_Time.at(volume_name).push_back( ( old_time*old_energy + step_time*step_energy )/( old_energy+step_energy ) );
        //std::cout << "else old energy: " << old_energy << " old time : " << old_time << 
         // " new energy : " << (fEnergy_Time.at(volume_name))[0] << " new time :" << (fEnergy_Time.at(volume_name))[1] << std::endl;

    }

    //std::cout << " long test : " << (fEnergy_Time.at(volume_name)).size() << std::endl;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunData::Reset()
{

    fEnergy_Time.clear();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
