//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// 
/// \file SteppingAction.cc
/// \brief Implementation of the SteppingAction class

#include "SteppingAction.hh"
#include "RunData.hh"
#include "myDetectorConstruction.hh"

#include "G4Step.hh"
#include "G4RunManager.hh"
#include "G4UserLimits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(
                      const myDetectorConstruction* detectorConstruction)
  : G4UserSteppingAction(),
    fDetConstruction(detectorConstruction)
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{ 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* step)
{
// Collect energy and track length step by step

  // get volume of the current step
  auto volume = step->GetPreStepPoint()->GetTouchableHandle()->GetVolume();
  auto volume_name = volume->GetName();
  
  // energy deposit
  // Info about steplimits
#if 0
  if (volume_name == "Silicon" || volume_name == "CoolPV")
  {
	G4Track track = *step->GetTrack();
	G4cout<<"My Step Limit is : "<<(fDetConstruction->GetUserLimits())->GetMaxAllowedStep(track)<<G4endl;
	G4cout<< "The particle is : "<<track.GetParticleDefinition()->GetParticleName()<<G4endl;
	G4cout<< "Vol: "<<volume_name<<G4endl;
	G4cout<< "StepLength: "<<step->GetStepLength()<<G4endl;
  }
#endif
  auto edep = step->GetTotalEnergyDeposit();
  
  auto postPoint = step->GetPostStepPoint();
  auto prePoint = step->GetPreStepPoint();
  G4float time = (postPoint->GetGlobalTime() + prePoint->GetGlobalTime())/2;
  auto x =( postPoint->GetPosition().x() + prePoint->GetPosition().x() )/2;
  auto y =( postPoint->GetPosition().y() + prePoint->GetPosition().y() )/2;
  auto z =( postPoint->GetPosition().z() + prePoint->GetPosition().z() )/2;

  /* G4float time = postPoint->GetGlobalTime() ; */
  /* auto x = postPoint->GetPosition().x() ; */
  /* auto y = postPoint->GetPosition().y() ; */
  /* auto z = postPoint->GetPosition().z() ; */

  auto real_cell_xsize = fDetConstruction->cell_SizeX/384;
  auto real_cell_ysize = fDetConstruction->cell_SizeY/312;
  auto real_xcell_number = fDetConstruction->xcell_number * 384;
  auto real_ycell_number = fDetConstruction->ycell_number * 312;

  auto layer_thickness = fDetConstruction->coolThickness
     				+fDetConstruction->absoThickness
				+fDetConstruction->siliconThickness
     				+fDetConstruction->gapThickness;
  G4int k = floor( z/layer_thickness ) + 1;
  //G4int i = floor( x/fDetConstruction->cell_SizeXY ) + fDetConstruction->xcell_number/2 + 1;
  //G4int j = floor( y/fDetConstruction->cell_SizeXY ) + fDetConstruction->ycell_number/2 + 1;
  G4int i = floor( x/real_cell_xsize ) + (real_xcell_number)/2 + 1;
  G4int j = floor( y/real_cell_ysize ) + (real_ycell_number)/2 + 1;

  //std::cout << "x: " << x << "  y: " << y << "  z: " << z << std::endl;
  //std::cout << "i: " << i << "  j: " << j << "  k: " << k << std::endl;

#if 0
  // step length
  G4double stepLength = 0.;
  if ( step->GetTrack()->GetDefinition()->GetPDGCharge() != 0. ) {
    stepLength = step->GetStepLength();
  }
#endif

  auto runData = static_cast<RunData*>
    (G4RunManager::GetRunManager()->GetNonConstCurrentRun());

  auto z_layer = z - (k-1)*layer_thickness ;
  /* double low_value = fDetConstruction->coolThickness + fDetConstruction->absoThickness; */
  /* double high_value = fDetConstruction->coolThickness + fDetConstruction->absoThickness + fDetConstruction->siliconThickness ; */
  double low_value =  fDetConstruction->absoThickness;
  double high_value = fDetConstruction->absoThickness + fDetConstruction->siliconThickness ;
  bool judge_sensitive = ( z_layer>=(low_value) ) && ( z_layer<=(high_value) ) ;
  //auto yushu = (-1*z) - floor( (-1*z)/layer_thickness ) * layer_thickness;

  bool judge_dep = edep>0.0;
  bool judge_outer = k>=1 && k<=fDetConstruction->nofLayers && i>=1 && i<=real_xcell_number && j>=1 && j<=real_ycell_number;
  bool judge_no_inner = !( i>(384-32) && i<(385+32) && j>(312-32) && j<(312+32) );

  if( judge_sensitive && judge_dep && judge_outer && judge_no_inner ){
  
     auto the_name = std::to_string(k) + "_" + std::to_string(i) + "_" + std::to_string(j);
     
     /* std::cout << " x : " << x << " y : " << y << " z : " << z << std::endl; */
     /* std::cout << " Step check : " << the_name << " energy : " << edep << std::endl; */ 

     runData->Update_Energy_Time(the_name, edep, time);
  }


}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
