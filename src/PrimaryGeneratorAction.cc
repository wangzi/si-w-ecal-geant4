//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// 
/// \file PrimaryGeneratorAction.cc
/// \brief Implementation of the PrimaryGeneratorAction class

//#pragma GCC diagnostic push
//#pragma 
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TROOT.h"

#include "PrimaryGeneratorAction.hh"

#include "G4RunManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"
#include "G4String.hh"

#include "Analysis.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction(const int& value, G4String name )
 : G4VUserPrimaryGeneratorAction(),
   fParticleGun(nullptr)
{

    eventNumber = value;
    inputName = name;

  //tot_ch = new TChain("Hits");
  //tot_ch->Add("../my_sprucing.root");
  //auto event_tree = tot_ch->CopyTree("eventNumber==1");
  /* G4int n_particle = 1; */
  /* fParticleGun  = new G4ParticleGun(n_particle); */
  //G4int nofParticles = event_tree->GetEntries();  
}



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

#if 0
  // This function is called at the begining of event
  // In order to avoid dependence of PrimaryGeneratorAction
  // on DetectorConstruction class we get world volume 
  // from G4LogicalVolumeStore
  //
  G4double worldZHalfLength = 0.;
  auto worldLV = G4LogicalVolumeStore::GetInstance()->GetVolume("World");

  // Check that the world volume has box shape
  G4Box* worldBox = nullptr;
  if (  worldLV ) {
    worldBox = dynamic_cast<G4Box*>(worldLV->GetSolid());
  }

  if ( worldBox ) {
    worldZHalfLength = worldBox->GetZHalfLength();  
  }
  else  {
    G4ExceptionDescription msg;
    msg << "World volume of box shape not found." << G4endl;
    msg << "Perhaps you have changed geometry." << G4endl;
    msg << "The gun will be place in the center.";
    G4Exception("PrimaryGeneratorAction::GeneratePrimaries()",
      "MyCode0002", JustWarning, msg);
  } 
#endif


  //auto input_file = TFile::Open("./my_sprucing.root");
  //auto hits_tree = (TTree*) input_file->Get("Hits"); 

  //event->GetEventID();
  //anEvent->
  //modified
  anEvent->SetEventID( eventNumber );
  /* anEvent->SetEventID( anEvent->GetEventID()); */

#if 1
  // Read into input file
  //G4String inputfile_name = 
    // "/disk303/lhcb/xuzh/full_sim/Upgrade_Ecal_Occupancy_Simulation/mytest/Low_Lumi_Current/Bs_PhiGamma_13102201/1/sim/mytuple_100ev_01.root";
  G4String inputfile_name = inputName;
  auto input_file = TFile::Open(inputfile_name);
  /* auto hits_tree = (TTree*) input_file->Get("GiGaGeo.PlaneDet.Tuple/Hits"); */    
  auto hits_tree = (TTree*) input_file->Get("Hits");    
  //std::cout << "Total tree number : " << hits_tree->GetEntries() << std::endl;

  //auto event_tree = hits_tree->CopyTree(Form("eventNumber==%i",eventID+1));
  auto event_tree = hits_tree->CopyTree(Form("eventNumber==%i", eventNumber));
  auto sub_event_number = event_tree->GetEntries();

  Int_t n;              event_tree->SetBranchAddress("n",&n);
  Float_t pid[20000];   event_tree->SetBranchAddress("pid",pid);
  Float_t x[20000];     event_tree->SetBranchAddress("x",x);
  Float_t y[20000];     event_tree->SetBranchAddress("y",y);
  Float_t z[20000];     event_tree->SetBranchAddress("z",z);
  Float_t t[20000];     event_tree->SetBranchAddress("t",t);
  Float_t px[20000];    event_tree->SetBranchAddress("px",px);
  Float_t py[20000];    event_tree->SetBranchAddress("py",py);
  Float_t pz[20000];    event_tree->SetBranchAddress("pz",pz);
  Float_t e[20000];     event_tree->SetBranchAddress("e",e);
 
  G4double zmax = 12520+26*5.5-2;
  G4double zmin = 12520;

  std::cout << "Sub event number : " << sub_event_number << std::endl;

  for(int i=0; i<sub_event_number; i++){
      event_tree->GetEntry(i);
              
      //std::cout << "In sub event " << i << "   the hits number is : " << n << std::endl; 
      
      for(int j=0; j<n; j++){
          
          //if( (pz[j]>0) && (z[j]<zmax) && (z[j]>zmin) ){
          if( (pz[j]>0) && (z[j]<zmax) ){
      
              //std::cout << "pid : " << pid[j] << " pz : " << pz[j]
                  //<< " x : " << x[j] << " y : " << y[j] << "  z : " << z[j] << std::endl;

              G4PrimaryParticle *pptmp = new G4PrimaryParticle(pid[j],
                      px[j], py[j], pz[j], e[j]);
              G4PrimaryVertex *pvtmp = new G4PrimaryVertex( x[j], 
                      y[j], z[j]-zmin, t[j]);
              pvtmp->SetPrimary(pptmp);
              anEvent->AddPrimaryVertex(pvtmp);
          
          }
      }
  }
#endif

#if 0
  // Read into input file
  //G4String inputfile_name = 
    // "/disk303/lhcb/xuzh/full_sim/Upgrade_Ecal_Occupancy_Simulation/mytest/Low_Lumi_Current/Bs_PhiGamma_13102201/1/sim/mytuple_100ev_01.root";
  G4String inputfile_name = inputName;
  auto input_file = TFile::Open(inputfile_name);
  auto hits_tree = (TTree*) input_file->Get("Gens");    
  //std::cout << "Total tree number : " << hits_tree->GetEntries() << std::endl;

  //auto event_tree = hits_tree->CopyTree(Form("eventNumber==%i",eventID+1));
  auto event_tree = hits_tree->CopyTree(Form("eventNumber==%i", eventNumber));
  auto sub_event_number = event_tree->GetEntries();

  std::cout << "input eventNumber : " << eventNumber << std::endl;

  Int_t pid; 		event_tree->SetBranchAddress("pid",&pid);
  Int_t id; 		event_tree->SetBranchAddress("id",&id);
  Int_t momid; 		event_tree->SetBranchAddress("momid",&momid);
  Int_t momPid; 		event_tree->SetBranchAddress("momPid",&momPid);
  Double_t x;		event_tree->SetBranchAddress("x",&x);
  Double_t y;		event_tree->SetBranchAddress("y",&y);
  Double_t z;		event_tree->SetBranchAddress("z",&z);
  Double_t t;		event_tree->SetBranchAddress("t",&t);
  Double_t px;		event_tree->SetBranchAddress("px",&px);
  Double_t py;		event_tree->SetBranchAddress("py",&py);
  Double_t pz;		event_tree->SetBranchAddress("pz",&pz);
  Double_t e;		event_tree->SetBranchAddress("e",&e);

  /* G4double zmax = 12520+26*8.8-2; */
  G4double zmax = 12520+26*9.0-2;
  G4double zmin = 12520;

  G4double light_speed = 299.792458; // mm/ns
  G4double pi_mass = 139.57; //MeV
  G4double e_mass = 0.5109989;
  G4double k_mass = 493.677;
  G4double p_mass = 938.272;

  std::cout << "Sub event number : " << sub_event_number << std::endl;

  for(int i=0; i<sub_event_number; i++){
      event_tree->GetEntry(i);
	
	double new_x = (12520-z)*px/pz+x;
	double new_y = (12520-z)*py/pz+y;

	auto judge1 = abs(pid)==11 || abs(pid)==22 || abs(pid)==2212 || abs(pid)==321 || abs(pid)==211;
	if( judge1==false ) { continue; }

	bool judge2 = ( abs(new_x)<10.1*384 && abs(new_y)<10.1*312 );
	if( judge2==false ) { continue; }

	bool judge3 = ( abs(new_x)<10.1*32 && abs(new_y)<10.1*32 );
	if( judge3==true ) { continue; }

	bool judge4 = ( pz>0) ;
	if( judge4==false ) { continue; }

	if( abs(pid)==22 && e>1000 ){
	   std::cout << " new x : " <<  new_x << " new y : " <<  new_y << " energy : " << e << std::endl;
	}

	G4PrimaryParticle *pptmp = new G4PrimaryParticle( pid, px, py, pz, e);
	G4PrimaryVertex *pvtmp = new G4PrimaryVertex( x, y, z-zmin, t);
	pvtmp->SetPrimary(pptmp);
	anEvent->AddPrimaryVertex(pvtmp);

  }
#endif
#if 0
  G4int pid=22;
  G4double fAlphaMax = 12*deg;
  G4double fAlphaMin = 2*deg;
  /* G4double cosAlpha = 1. - G4UniformRand()*(1.- std::cos(fAlphaMax)); */
  G4double cosAlpha = std::cos(fAlphaMin) - G4UniformRand()*(std::cos(fAlphaMin)- std::cos(fAlphaMax));
  G4double sinAlpha = std::sqrt(1. - cosAlpha*cosAlpha);
  G4double psi      = CLHEP::twopi*G4UniformRand();  //psi uniform in [0, 2*pi]
  /* G4double cosAlpha = 1; */
  /* G4double sinAlpha = 0; */
  /* G4double psi = 0; */
  //G4ThreeVector dir(sinAlpha*std::cos(psi),sinAlpha*std::sin(psi),cosAlpha);
  //G4ThreeVector dir(0,0,1);


  G4double p = ( ((G4UniformRand()*399)+1)*1000 )*MeV;
  G4double px = p*sinAlpha*std::cos(psi);
  G4double py = p*sinAlpha*std::sin(psi);
  G4double pz = p*cosAlpha;
  /* G4double px = 0; */
  /* G4double py = 0; */
  /* G4double pz = p; */

  
  /* G4ThreeVector dir(sinAlpha*std::cos(psi),sinAlpha*std::sin(psi),cosAlpha); */
  //set energy
  //
  /* fParticleGun->SetInitialValues(); */
  /* fParticleGun->SetParticleEnergy( ( ((G4UniformRand()*99)+1)*1000 )*MeV); */
  /* fParticleGun->SetParticlePosition(G4ThreeVector(500,0,-12520)); */
  /* fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0,0,1)); */
  /* fParticleGun->GeneratePrimaryVertex(anEvent); */

  /* G4int nofParticles = event_tree->GetEntries(); */
  /* fParticleGun = new G4ParticleGun(nofParticles); */
  
  // Set gun position
  /* fParticleGun->SetParticlePosition(G4ThreeVector(100., 100., 0.)); */
  /* fParticleGun->GeneratePrimaryVertex(anEvent); */

  // default particle kinematic
  //
  /* auto particleDefinition */ 
    //= G4ParticleTable::GetParticleTable()->FindParticle("e-");
    /* = G4ParticleTable::GetParticleTable()->FindParticle("pi-"); */
  /* fParticleGun->SetParticleDefinition(particleDefinition); */
  /* fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.)); */
  /* fParticleGun->SetParticleEnergy(50000.*MeV); */

  //G4PrimaryParticle *pptmp = new G4PrimaryParticle( pid, px, py, pz, e);
  /* G4PrimaryVertex *pvtmp = new G4PrimaryVertex( 10.1*100+10.1*G4UniformRand(), 10.1*G4UniformRand(), -12520, 0); */
  
  G4PrimaryParticle *pptmp = new G4PrimaryParticle( pid, px, py, pz);
  G4PrimaryVertex *pvtmp = new G4PrimaryVertex( 0, 0, -12520, 0);
  pvtmp->SetPrimary(pptmp);
  anEvent->AddPrimaryVertex(pvtmp);
#endif

#if 0
  auto analysisManager = G4AnalysisManager::Instance();
  
  analysisManager->FillNtupleIColumn(2, 0, anEvent->GetEventID());
  /* analysisManager->FillNtupleFColumn(2, 1, fParticleGun->GetParticleEnergy() ); */
  /* analysisManager->FillNtupleFColumn(2, 2, 0 ); */
  /* analysisManager->FillNtupleFColumn(2, 3, 0 ); */
  /* analysisManager->FillNtupleFColumn(2, 4, fParticleGun->GetParticleEnergy() ); */
  
  /* analysisManager->FillNtupleIColumn(2, 0, eventNumber); */
  analysisManager->FillNtupleFColumn(2, 1, sqrt(p*p) );
  analysisManager->FillNtupleFColumn(2, 2, px );
  analysisManager->FillNtupleFColumn(2, 3, py );
  analysisManager->FillNtupleFColumn(2, 4, pz );
  analysisManager->AddNtupleRow(2);  
#endif

#if 0
  G4int nofParticles = event_tree->GetEntries();
  fParticleGun = new G4ParticleGun(nofParticles);
  
  // Set gun position
  fParticleGun->SetParticlePosition(G4ThreeVector(100., 100., 0.));
  fParticleGun->GeneratePrimaryVertex(anEvent);

  // default particle kinematic
  //
  auto particleDefinition 
    //= G4ParticleTable::GetParticleTable()->FindParticle("e-");
    = G4ParticleTable::GetParticleTable()->FindParticle("pi-");
  fParticleGun->SetParticleDefinition(particleDefinition);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(50000.*MeV);
#endif


#if 0

//vertex A uniform on a cylinder
  //
  const G4double r = 2*mm;
  const G4double zmax = 8*mm;
  //
  G4double alpha = twopi*G4UniformRand();     //alpha uniform in (0, 2*pi)
  G4double ux = std::cos(alpha);
  G4double uy = std::sin(alpha);
  G4double z = zmax*(2*G4UniformRand() - 1);  //z uniform in (-zmax, +zmax)
  G4ThreeVector positionA(r*ux,r*uy,z);
  G4double timeA = 0*s;
  // 
  G4PrimaryVertex* vertexA = new G4PrimaryVertex(positionA, timeA);
  
  //particle 1 at vertex A
  //
  G4ParticleDefinition* particleDefinition
           = G4ParticleTable::GetParticleTable()->FindParticle("geantino");
  G4PrimaryParticle* particle1 = new G4PrimaryParticle(particleDefinition);
  particle1->SetMomentumDirection(G4ThreeVector(ux,uy,0));    
  particle1->SetKineticEnergy(1*MeV);
  //
  vertexA->SetPrimary(particle1);
  event->AddPrimaryVertex(vertexA);

  //vertex (B) symetric to vertex A
  //
  alpha += pi;
  ux = std::cos(alpha);
  uy = std::sin(alpha);
  G4ThreeVector positionB(r*ux,r*uy,z);
  G4double timeB = 1*s;
  // 
  G4PrimaryVertex* vertexB = new G4PrimaryVertex(positionB, timeB);

  //particles 2 and 3 at vertex B
  //
  const G4double dalpha = 10*deg;
  ux = std::cos(alpha + dalpha);
  uy = std::sin(alpha + dalpha);
  G4PrimaryParticle* particle2 = new G4PrimaryParticle(particleDefinition);
  particle2->SetMomentumDirection(G4ThreeVector(ux,uy,0));
  particle2->SetKineticEnergy(1*keV);
  //
  ux = std::cos(alpha - dalpha);
  uy = std::sin(alpha - dalpha);
  G4PrimaryParticle* particle3 = new G4PrimaryParticle(particleDefinition);
  particle3->SetMomentumDirection(G4ThreeVector(ux,uy,0));
  particle3->SetKineticEnergy(1*GeV);
  //
  vertexB->SetPrimary(particle2);
  vertexB->SetPrimary(particle3);
  event->AddPrimaryVertex(vertexB);

#endif

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

