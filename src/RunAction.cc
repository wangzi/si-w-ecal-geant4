//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file RunAction.cc
/// \brief Implementation of the RunAction class

#include "RunAction.hh"
#include "RunData.hh"
#include "Analysis.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include "TTree.h"
#include "TFile.h"

#include <ctime>


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::RunAction( const int& value )
 : G4UserRunAction(),
    eventNumber( value )
{ 
  // set printing event number per each event
  G4RunManager::GetRunManager()->SetPrintProgress(1);     

  // Create analysis manager
  // The choice of analysis technology is done via selectin of a namespace
  // in Analysis.hh
  auto analysisManager = G4AnalysisManager::Instance();
  G4cout << "Using " << analysisManager->GetType() << G4endl;

  // Create directories 
  //analysisManager->SetNtupleDirectoryName("ntuple");
  analysisManager->SetVerboseLevel(1);
  analysisManager->SetNtupleMerging(true);
  // Note: merging ntuples is available only with Root output

  analysisManager->SetFirstNtupleId(1);

  // Creating ntuple
  //
  analysisManager->CreateNtuple("geant4_tree", "Edep and Time");
  analysisManager->CreateNtupleIColumn(1,"EventNumber");
  analysisManager->CreateNtupleSColumn(1,"cell_ID");
  analysisManager->CreateNtupleFColumn(1,"Energy");
  analysisManager->CreateNtupleFColumn(1,"Time");
  analysisManager->FinishNtuple();

#if 0
  analysisManager->CreateNtuple("gen_tree", "E");
  analysisManager->CreateNtupleIColumn(2,"EventNumber");
  analysisManager->CreateNtupleFColumn(2,"E");
  analysisManager->CreateNtupleFColumn(2,"px");
  analysisManager->CreateNtupleFColumn(2,"py");
  analysisManager->CreateNtupleFColumn(2,"pz");
  analysisManager->FinishNtuple();
#endif 
  

  analysisManager->CreateNtuple("track_contain", "Pt and Vtx");
  analysisManager->CreateNtupleIColumn(2,"EventNumber");
  analysisManager->CreateNtupleIColumn(2,"pid");
  analysisManager->CreateNtupleIColumn(2,"id");
  analysisManager->CreateNtupleIColumn(2,"mom_id");

  analysisManager->CreateNtupleFColumn(2,"px");
  analysisManager->CreateNtupleFColumn(2,"py");
  analysisManager->CreateNtupleFColumn(2,"pz");
  analysisManager->CreateNtupleFColumn(2,"e");
  
  analysisManager->CreateNtupleFColumn(2,"x");
  analysisManager->CreateNtupleFColumn(2,"y");
  analysisManager->CreateNtupleFColumn(2,"z");
  analysisManager->CreateNtupleFColumn(2,"t");
  
  analysisManager->CreateNtupleSColumn(2,"Creato_Name");

  analysisManager->FinishNtuple();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{
  delete G4AnalysisManager::Instance();  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4Run* RunAction::GenerateRun()
{
  return (new RunData);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::BeginOfRunAction(const G4Run* run)
{ 
  G4cout << "### Run " << run->GetRunID() << " start." << G4endl;

  //inform the runManager to save random number seed
  //G4RunManager::GetRunManager()->SetRandomNumberStore(true);
  
  // Get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

  // Open an output file
  //
  /* G4String fileName = Form("tmp/output_sensors_response_%i", run->GetRunID()); */
  G4String fileName = Form("tmp/output_sensors_response_%i", eventNumber);
  analysisManager->OpenFile(fileName);

  // Read into input file
  //G4String inputfile_name = "./my_sprucing.root";
  //auto input_file = TFile::Open(inputfile_name);
  //hits_tree = (TTree*) input_file->Get("Hits");              

  //std::cout << " Tree Entries : " << hits_tree->GetEntries() << std::endl;

  auto seed=time(0);
  G4Random::setTheSeed(seed,3);
  G4Random::showEngineStatus();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run* /*aRun*/)
{
  
  //
  auto analysisManager = G4AnalysisManager::Instance();

  // save histograms & ntuple
  //
  analysisManager->Write();
  analysisManager->CloseFile();

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
