//
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include "TrackingAction.hh"

#include "myDetectorConstruction.hh"
//#include "Run.hh"
#include "PrimaryGeneratorAction.hh"
#include "EventAction.hh"
//#include "HistoManager.hh"

#include "G4RunManager.hh"
#include "G4PhysicalConstants.hh"
#include "G4Track.hh"
#include "G4Positron.hh"

#include "RunData.hh"
#include "Analysis.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

TrackingAction::TrackingAction(myDetectorConstruction* det)
:G4UserTrackingAction(),detector(det)
{ 
   //auto analysisManager = G4AnalysisManager::Instance();
}
 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrackingAction::PreUserTrackingAction(const G4Track* track)
{

   auto runData = static_cast<RunData*>
	(G4RunManager::GetRunManager()->GetNonConstCurrentRun());
  

   if((track->GetVertexPosition()).z()>-13000 && (track->GetVertexPosition()).z()<0 && track->GetVertexKineticEnergy()>5 && (track->GetMomentum()).z()>0 ){
   //if( (track->GetVertexPosition()).z()<0 ){

#if 0 
	std::cout << "************************************" << std::endl;
	std::cout << " Fill Tracking " << std::endl;
	std::cout << " Gen type : " << track->GetCreatorModelName() << " Particle pid : " 
	   << track->GetParticleDefinition()->GetPDGEncoding()  
	   << " ID : " << track->GetTrackID() 
	   << " mom ID " <<  track->GetParentID() << std::endl;
	std::cout << "Tracking Px : " << (track->GetMomentum()).x() << " Py : " << (track->GetMomentum()).y()  << " Pz : " << (track->GetMomentum()).z() 
	   << " E : " << track->GetVertexKineticEnergy() << std::endl;
	std::cout << "Tracking x : " << (track->GetVertexPosition()).x() << " y : " << (track->GetVertexPosition()).y()  
	   << " z : " << (track->GetVertexPosition()).z() << std::endl;

#endif
   }


	//const G4DynamicParticle* aParticle = track->GetDynamicParticle();
	//G4double E = aParticle->GetKineticEnergy();
	//if(E<1){
	//   aParticleChange.ProposeTrackStatus(fStopAndKill)
	//}

	//G4double E = track->GetKineticEnergy();
	//if(E<1){
	//   track->SetTrackStatus( fStopAndKill );
	//}

   // get analysis manager
   auto analysisManager = G4AnalysisManager::Instance();

   //if( (track->GetVertexPosition()).z()>-13000 && (track->GetVertexPosition()).z()<0 ){
   if( ((track->GetVertexPosition()).z()>-13000) && 
	   ((track->GetVertexPosition()).z()<0) && 
	   (track->GetVertexKineticEnergy()>5) && 
	   ((track->GetMomentum()).z()>0) ){

	analysisManager->FillNtupleIColumn(2, 0, runData->GetNowEventID() );
	analysisManager->FillNtupleIColumn(2, 1, track->GetParticleDefinition()->GetPDGEncoding() );
	analysisManager->FillNtupleIColumn(2, 2, track->GetTrackID() );
	analysisManager->FillNtupleIColumn(2, 3, track->GetParentID() );

	analysisManager->FillNtupleFColumn(2, 4, (track->GetMomentum()).x() );
	analysisManager->FillNtupleFColumn(2, 5, (track->GetMomentum()).y() );
	analysisManager->FillNtupleFColumn(2, 6, (track->GetMomentum()).z() );
	analysisManager->FillNtupleFColumn(2, 7, track->GetVertexKineticEnergy() );

	analysisManager->FillNtupleFColumn(2, 8, (track->GetVertexPosition()).x() );
	analysisManager->FillNtupleFColumn(2, 9, (track->GetVertexPosition()).y() );
	analysisManager->FillNtupleFColumn(2, 10, (track->GetVertexPosition()).z() );
	analysisManager->FillNtupleFColumn(2, 11, track->GetGlobalTime() );

	analysisManager->FillNtupleSColumn(2, 12, track->GetCreatorModelName() );

	analysisManager->AddNtupleRow(2);

   }



}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void TrackingAction::PostUserTrackingAction(const G4Track* track)
{ 
   

#if 0
  //compute leakage
  //  
  // if not at World boundaries, return
  if (track->GetVolume() != detector->GetPvolWorld()) return;
    
  //get position
  G4double x = (track->GetPosition()).x();
  G4double xlimit = 0.5*(detector->GetCalorThickness());
  G4int icase = 2;
  if (x >= xlimit) icase = 0;
  else if (x <= -xlimit) icase = 1;
  
  //get particle energy
  G4double Eleak = track->GetKineticEnergy();
  if (track->GetDefinition() == G4Positron::Positron())
     Eleak += 2*electron_mass_c2;
#endif

#if 0
  //sum leakage
  Run* run = static_cast<Run*>(
             G4RunManager::GetRunManager()->GetNonConstCurrentRun());
  run->DetailedLeakage(icase,Eleak);    
#endif
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


